/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This package indexes a document corpus.
 *
 * <p>
 * <i>Search Engines</i> is a course of the <a href="https://lauree.dei.unipd.it/lauree-magistrali/computer-engineering/"
 * target="_blank">Master Degree in Computer Engineering</a> of the  <a href="https://www.dei.unipd.it/en/"
 * target="_blank">Department of Information Engineering</a>, <a href="https://www.unipd.it/en/"
 * target="_blank">University of Padua</a>, Italy. </p>
 *
 * <p>
 * <i>Search Engines</i> is part of the teaching activities
 * of the <a href="http://iiia.dei.unipd.it/" target="_blank">Intelligent Interactive Information Access (IIIA)
 * Hub</a>.
 * </p>
 *
 * <p>Copyright and license information can be found in the file LICENSE.  Additional information can be found in the
 * file NOTICE.</p>
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.00
 * @since 1.00
 */
package it.unipd.dei.se.index;