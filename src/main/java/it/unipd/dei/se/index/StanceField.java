package it.unipd.dei.se.index;

import it.unipd.dei.se.parse.ParsedDocument;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;

import java.io.Reader;

public class StanceField extends Field {
    /**
     * The type of the document body field
     */
    private static final FieldType STANCE_TYPE = new FieldType();

    static {
        STANCE_TYPE.setStored(true);
    }


    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public StanceField(final Reader value) {
        super(ParsedDocument.FIELDS.STANCE, value, STANCE_TYPE);
    }

    /**
     * Create a new field for the body of a document.
     *
     * @param value the contents of the body of a document.
     */
    public StanceField(final String value) {
        super(ParsedDocument.FIELDS.PREMISES, value, STANCE_TYPE);
    }
}
