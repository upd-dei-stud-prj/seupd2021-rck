/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.index;

import it.unipd.dei.se.parse.DocumentParser;
import it.unipd.dei.se.parse.ParsedDocument;
import it.unipd.dei.se.parse.RckToucheParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;

/**
 * Indexes documents processing a whole directory tree.
 */
public class DirectoryIndexer {

    /**
     * One megabyte
     */
    private static final int MBYTE = 1024 * 1024;

    /**
     * The index writer.
     */
    private final IndexWriter writer;

    /**
     * The class of the {@code DocumentParser} to be used.
     */
    private final Class<? extends DocumentParser> parserClass;

    /**
     * The directory (and sub-directories) where documents are stored.
     */
    private final Path docsDir;
    /**
     * The extension of the files to be indexed.
     */
    private final String extension;
    /**
     * The charset used for encoding documents.
     */
    private final Charset charset;
    /**
     * The total number of documents expected to be indexed.
     */
    private final long expectedDocs;
    /**
     * The start instant of the indexing.
     */
    private final long start;
    /**
     * The total number of indexed files.
     */
    private long filesCount;
    /**
     * The total number of indexed documents.
     */
    private long docsCount;
    /**
     * The total number of indexed bytes
     */
    private long bytesCount;
    /**
     * Creates a new indexer.
     *
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsPath        the directory from which documents have to be read.
     * @param extension       the extension of the files to be indexed.
     * @param charsetName     the name of the charset used for encoding documents.
     * @param expectedDocs    the total number of documents expected to be indexed
     * @param parserClass           the class of the {@code DocumentParser} to be used.
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public DirectoryIndexer(
            final Analyzer analyzer,
            final Similarity similarity,
            final int ramBufferSizeMB,
            final String indexPath,
            final String docsPath,
            final String extension,
            final String charsetName,
            final long expectedDocs,
            final Class<? extends DocumentParser> parserClass
    )
    {
        if (parserClass == null) {
            throw new NullPointerException("Document parser class cannot be null.");
        }

        this.parserClass = parserClass;

        if (analyzer == null) {
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig writerConfig = new IndexWriterConfig(analyzer);
        writerConfig.setSimilarity(similarity);
        writerConfig.setRAMBufferSizeMB(ramBufferSizeMB);
        writerConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        writerConfig.setCommitOnClose(true);

        if (indexPath == null) {
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);

        // if the directory does not already exist, create it
        if (Files.notExists(indexDir)) {
            try {
                Files.createDirectory(indexDir);
            } catch (Exception e) {
                throw new IllegalArgumentException("Unable to create directory " + indexDir.toAbsolutePath().toString() + " - " + e.getMessage());
            }
        }
        if (!Files.isWritable(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("Index directory %s cannot be written.", indexDir.toAbsolutePath().toString()));
        }
        if (!Files.isDirectory(indexDir)) {
            throw new IllegalArgumentException(
                    String.format("%s expected to be a directory where to write the index.", indexDir.toAbsolutePath().toString())
            );
        }
        if (docsPath == null) {
            throw new NullPointerException("Documents path cannot be null.");
        }
        if (docsPath.isEmpty()) {
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);

        if (!Files.isReadable(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("Documents directory %s cannot be read.", docsDir.toAbsolutePath().toString())
            );
        }
        if (!Files.isDirectory(docsDir)) {
            throw new IllegalArgumentException(
                    String.format("%s expected to be a directory of documents.", docsDir.toAbsolutePath().toString())
            );
        }

        this.docsDir = docsDir;

        if (extension == null) {
            throw new NullPointerException("File extension cannot be null.");
        }
        if (extension.isEmpty()) {
            throw new IllegalArgumentException("File extension cannot be empty.");
        }
        this.extension = extension;

        if (charsetName == null) {
            throw new NullPointerException("Charset name cannot be null.");
        }
        if (charsetName.isEmpty()) {
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }
        try {
            charset = Charset.forName(charsetName);
        }
        catch (Exception e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
        }

        if (expectedDocs <= 0) {
            throw new IllegalArgumentException("The expected number of documents to be indexed cannot be less than or equal to zero.");
        }

        this.expectedDocs = expectedDocs;
        this.docsCount = 0;
        this.bytesCount = 0;
        this.filesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), writerConfig);
        }
        catch (IOException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to create the index writer in directory %s: %s.", indexDir.toAbsolutePath().toString(), e.getMessage()), e);
        }

        this.start = System.currentTimeMillis();
    }

    /**
     * Indexes the documents.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {

        System.out.printf("%n#### Start indexing ####%n");

        Files.walkFileTree(docsDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().endsWith(extension)) {

                    DocumentParser parser = DocumentParser.create(parserClass, Files.newBufferedReader(file, charset));

                    bytesCount += Files.size(file);
                    filesCount += 1;
                    Document doc = null;

                    final HashMap<String, String> duplicates = new HashMap<>();

                    for (ParsedDocument pd : parser) {

                        if(duplicates.containsKey(pd.getId()))
                            continue;

                        duplicates.put(pd.getId(), "duplicate");
                        doc = new Document();
                        // add the document identifier
                        doc.add(new StringField(ParsedDocument.FIELDS.ID, pd.getId(), Field.Store.YES));
                        // add the document stance WITHOUT ANALYZING IT
                        doc.add(new StringField(ParsedDocument.FIELDS.STANCE, pd.getStance(), Field.Store.YES));
                        // add the document body
                        doc.add(new BodyField(pd.getBody()));
                        doc.add(new PremisesField(pd.getPremisesText()));
                        writer.addDocument(doc);
                        docsCount++;

                        // print progress every 10000 indexed documents
                        if (docsCount % 10000 == 0) {
                            System.out.printf(
                                    "%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
                                    docsCount, filesCount, bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);
                        }
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });

        writer.commit();
        writer.close();

        if (docsCount != expectedDocs) {
            System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
        }

        System.out.printf(
                "%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
                bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

        System.out.printf("#### Indexing complete ####%n");
    }

    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {

        //ram buffer size in megabyte
        final int ramBuffer = 512;
        final String docsPath = "docs";
        final String indexPath = "experiment/index-LMD-StandardTokenizer-LowerCaseFilter-StopFilter-PorterStemFilter";

        final String extension = "json";
        final int expectedDocs = 387740;
        final String charsetName = "ISO-8859-1";

        final Analyzer a = CustomAnalyzer
                .builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .addTokenFilter(StopFilterFactory.class)
                .addTokenFilter(PorterStemFilterFactory.class).build();

        DirectoryIndexer i = new DirectoryIndexer(a,
                //new BM25Similarity(),
                new LMDirichletSimilarity(),
                ramBuffer,
                indexPath,
                docsPath,
                extension,
                charsetName,
                expectedDocs,
                RckToucheParser.class);

        i.index();
    }

}
