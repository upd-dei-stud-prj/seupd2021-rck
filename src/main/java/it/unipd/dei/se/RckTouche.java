/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se;


import it.unipd.dei.se.index.DirectoryIndexer;
import it.unipd.dei.se.parse.RckToucheParser;
import it.unipd.dei.se.search.Searcher;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.shingle.ShingleFilterFactory;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;

/**
 * TOUCHE
 */
public class RckTouche {

    /**
     * Main method of the class.
     *
     * @param args command line arguments. If provided, {@code args[0]} contains the path the the index directory;
     *             {@code args[1]} contains the path to the run file.
     * @throws Exception if something goes wrong while indexing and searching.
     */
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 256;
        final String docsPath = "docs";
        final String indexPath = "experiment/index-stop-nostem";

        final String extension = "json";
        final int expectedDocs = 387740;
        final String charsetName = "ISO-8859-1";

        final Analyzer a = CustomAnalyzer
                .builder()
                .withTokenizer(StandardTokenizerFactory.class) //tokenizer
                .addTokenFilter(LowerCaseFilterFactory.class) //all to lowercase
                .addTokenFilter(StopFilterFactory.class) //remove stop words

                //Shingle/Ngram
                .addTokenFilter(NGramFilterFactory.class,"3","3")
                .addTokenFilter(ShingleFilterFactory.class,"2")
                .build();

        final Similarity sim = new BM25Similarity();
        final String topics = "/topics/topics-task-1-only-titles.xml";
        final String runPath = "experiment";
        final String runID = "seupd2021-rck-touche";

        final int maxDocsRetrieved = 1000;
        final int expectedTopics = 50;

        // indexing
        final DirectoryIndexer i = new DirectoryIndexer(
                a, //analyzer
                sim, //similarity
                ramBuffer, //buffersize
                indexPath, //path to store the index
                docsPath, //path where documents are stored
                extension, //documents extension
                charsetName, //encoding of documents
                expectedDocs, //number of expected documents
                RckToucheParser.class //parser class
        );
        i.index();

        // searching
        final Searcher s = new Searcher(
                a,
                sim,
                indexPath,
                topics,
                expectedTopics,
                runID, runPath,
                maxDocsRetrieved
        );
        s.search();

    }

}
