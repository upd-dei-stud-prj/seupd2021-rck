/*
 *  Copyright 2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Represents a topic parser.
 */
public abstract class TopicParser
        implements Iterator<ParsedTopic>, Iterable<ParsedTopic> {

    /**
     * Indicates whether there is another {@code ParsedDocument} to return.
     */
    protected boolean next = true;

    /**
     * The reader to be used to parse document(s).
     */
    protected final Reader reader;

    /**
     * Creates a new document parser.
     * @param reader the reader to the document(s) to be parsed.
     * @throws NullPointerException if {@code reader} is {@code null}.
     */
    protected TopicParser(final Reader reader) {

        if (reader == null) {
            throw new NullPointerException("Reader cannot be null.");
        }
        this.reader = reader;
    }

    @Override
    public final Iterator<ParsedTopic> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return next;
    }

    @Override
    public final ParsedTopic next() {

        if (!next) {
            throw new NoSuchElementException("No more documents to parse.");
        }

        try {
            return parse();
        }
        finally {
            try {
                // we reached the end of the file
                if (!next) {
                    reader.close();
                }
            }
            catch (IOException e) {
                throw new IllegalStateException("Unable to close the reader.", e);
            }
        }

    }

    /**
     * Creates a new {@code DocumentParser}.
     * <p>
     * It assumes the {@code DocumentParser} has a single-parameter constructor which takes a {@code Reader} as input.
     *
     * @param parserClass the class of the document parser to be instantiated.
     * @param in  the reader to the document(s) to be parsed.
     * @return a new instance of {@code DocumentParser} for the given class.
     * @throws NullPointerException  if {@code parserClass} and/or {@code in} are {@code null}.
     * @throws IllegalStateException if something goes wrong in instantiating the class.
     */
    public static TopicParser create(Class<? extends TopicParser> parserClass, Reader in) {

        if (parserClass == null) {
            throw new NullPointerException("Document parser class cannot be null.");
        }

        if (in == null) {
            throw new NullPointerException("Reader cannot be null.");
        }


        try {
            return parserClass.getConstructor(Reader.class).newInstance(in);
        }
        catch (Exception e) {
            throw new IllegalStateException(String.format("Unable to instantiate document parser %s.", parserClass.getName()), e);
        }

    }

    /**
     * Performs the actual parsing of the document.
     *
     * @return the parsed document.
     */
    protected abstract ParsedTopic parse();
}
