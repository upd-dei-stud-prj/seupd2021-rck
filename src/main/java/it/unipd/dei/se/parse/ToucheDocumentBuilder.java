package it.unipd.dei.se.parse;

public class ToucheDocumentBuilder {
    private String id;
    private String body;
    private String conclusion = "";
    private String stance = "";
    private String sourceTitle = "";
    private String discussionTitle = "";
    private String premisesText = "";
    private String authorRole = "";
    private String author = "";
    private String authorOrg = "";
    private String sourceText = "";
    private String mode = "";
    private String topic = "";
    private String sourceDomain = "";

    public ToucheDocumentBuilder(String id){
        setId(id);
    }

    public ToucheDocumentBuilder(){}

    public ToucheDocumentBuilder setId(String id){
        this.id = id;
        return this;
    }

    public ToucheDocumentBuilder setBody(String body){
        this.body = body;
        return this;
    }

    public ToucheDocumentBuilder setConclusion(String conclusion){
        this.conclusion = conclusion;
        return this;
    }

    public ToucheDocumentBuilder setStance(String stance){
        this.stance = stance;
        return this;
    }

    public ToucheDocumentBuilder setSourceTitle(String sourceTitle){
        this.sourceTitle = sourceTitle;
        return this;
    }

    public ToucheDocumentBuilder setDiscussionTitle(String discussionTitle){
        this.discussionTitle = discussionTitle;
        return this;
    }

    public ToucheDocumentBuilder setPremisesText(String ps){
        this.premisesText = ps;
        return this;
    }
    public ToucheDocumentBuilder setAuthorRole(String authorRole){
        this.authorRole = authorRole;
        return this;
    }
    public ToucheDocumentBuilder setAuthor(String author){
        this.author = author;
        return this;
    }
    public ToucheDocumentBuilder setAuthorOrg(String authorOrg){
        this.authorOrg = authorOrg;
        return this;
    }
    public ToucheDocumentBuilder setSourceText(String sourceText){
        this.sourceText = sourceText;
        return this;
    }
    public ToucheDocumentBuilder setMode(String mode){
        this.mode = mode;
        return this;
    }
    public ToucheDocumentBuilder setTopic(String topic){
        this.topic = topic;
        return this;
    }
    public ToucheDocumentBuilder setSourceDomain(String sourceDomain){
        this.sourceDomain = sourceDomain;
        return this;
    }
    public ParsedDocument build(){
        return new ParsedDocument(
                id,
                body,
                conclusion,
                stance,
                sourceTitle,
                discussionTitle,
                premisesText,
                authorRole,
                authorOrg,
                author,
                sourceText,
                mode,
                topic,
                sourceDomain
        );
    }
}