package it.unipd.dei.se.parse;

import it.unipd.dei.se.search.Searcher;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;

public class RckTopicsReader {

    public QualityQuery[] readQueries(Reader in) throws IOException, SAXException, ParserConfigurationException {
        final RckTopicParser parser = new RckTopicParser(in);
        final ArrayList<QualityQuery> queries = new ArrayList<>();
        for(ParsedTopic t: parser){
            final HashMap<String, String> contents = new HashMap<>();
            contents.put(Searcher.TOPIC_FIELDS.TITLE, t.getTitle());
            contents.put(Searcher.TOPIC_FIELDS.DESCRIPTION, t.getDescription());
            queries.add(new QualityQuery(t.getNumber(), contents));
        }
        return queries.toArray(new QualityQuery[0]);
    }
}
