/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;
import java.util.HashMap;

/**
 * The TOUCHE document parser
 */
public class RckToucheParser extends DocumentParser{
    private static final String ID = "id";
    private static final String CONCLUSION = "conclusion";
    private static final String STANCE = "stance";
    private static final String TEXT = "text";
    private static final String AUTHOR_ROLE = "authorRole";
    private static final String AUTHOR = "author";
    private static final String AUTHOR_ORG = "authorOrganization";
    private static final String SOURCE_TITLE = "sourceTitle";
    private static final String DISCUSSION_TITLE = "discussionTitle";
    private static final String SOURCE_TEXT = "sourceText";
    private static final String MODE = "mode";
    private static final String TOPIC = "topic";
    private static final String SOURCE_DOMAIN = "sourceDomain";

    /**
     * The currently parsed document
     */
    private ParsedDocument document = null;
    private final JsonParser parser;
    private JsonToken token;
    /**
     * Creates a new TOUCHE Corpus document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
	 * @throws IOException if something went wrong in I/O operation.
     */
    public RckToucheParser(final Reader in) throws IOException {
        super(new BufferedReader(in));
        JsonFactory factory = new JsonFactory();
        parser = factory.createParser(in);

        //get next token until array of data starts
        token = parser.nextToken();
        while(token != JsonToken.START_ARRAY)
            token = parser.nextToken();
    }

    @Override
    public boolean hasNext() {
        try{
            token = parser.nextToken();
            //we have exhausted the array, no more documents to parse
            if(token == JsonToken.END_ARRAY)
                return false;

            //store in memory only next document
            ObjectMapper mapper = new ObjectMapper();
            JsonNode doc = mapper.readTree(parser);

            //initialize document builder
            ToucheDocumentBuilder builder = new ToucheDocumentBuilder();
            builder.setId(doc.get(ID).asText());
            builder.setConclusion(doc.get(CONCLUSION).asText());

            //get premises and context fields from document
            JsonNode premises = doc.get("premises").get(0);
            JsonNode context = doc.get("context");

            //set document fields
            //documents with different origins may have different fields, but many fields are in common
            if(context.has(AUTHOR_ROLE))
                builder.setAuthorRole(context.get(AUTHOR_ROLE).asText());

            if(context.has(AUTHOR))
                builder.setAuthor(context.get(AUTHOR).asText());

            if(context.has(AUTHOR_ORG))
                builder.setAuthorOrg(context.get(AUTHOR_ORG).asText());

            if(context.has(DISCUSSION_TITLE))
                builder.setDiscussionTitle(context.get(DISCUSSION_TITLE).asText());

            if(context.has(TOPIC))
                builder.setTopic(context.get(TOPIC).asText());

            builder.setStance(premises.get(STANCE).asText());
            builder.setPremisesText(premises.get(TEXT).asText());
            builder.setSourceTitle(context.get(SOURCE_TITLE).asText());
            builder.setSourceText(context.get(SOURCE_TEXT).asText());
            builder.setBody(context.get(SOURCE_TEXT).asText());
            builder.setMode(context.get(MODE).asText());
            builder.setSourceDomain(context.get(SOURCE_DOMAIN).asText());

            document = builder.build();
        }

        catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected final ParsedDocument parse() {
        return document;
    }

    /**
     * Main method of the class. Just to count all documents and make sure parser behaves correctly.
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {
        //parliamentary has 48 documents, we show that all 48 are retrieved
        Reader reader = new FileReader("docs/parliamentary.json");
        RckToucheParser p = new RckToucheParser(reader);

        //there are duplicate documents
        HashMap<String, String> idMap = new HashMap<>();
        int count = 0;
        for(ParsedDocument d: p){
            String id = d.getId();
            String inMap = idMap.getOrDefault(id, "");
            if(inMap.isEmpty())
                idMap.put(id, id);
            else
                count++;
        }

        reader = new FileReader("docs/idebate.json");
        p = new RckToucheParser(reader);
        for(ParsedDocument d: p){
            String id = d.getId();
            String inMap = idMap.getOrDefault(id, "");
            if(inMap.isEmpty())
                idMap.put(id, id);
            else
                count++;
        }

        reader = new FileReader("docs/debatewise.json");
        p = new RckToucheParser(reader);
        for(ParsedDocument d: p){
            String id = d.getId();
            String inMap = idMap.getOrDefault(id, "");
            if(inMap.isEmpty())
                idMap.put(id, id);
            else
                count++;
        }

        reader = new FileReader("docs/debatepedia.json");
        p = new RckToucheParser(reader);
        for(ParsedDocument d: p){
            String id = d.getId();
            String inMap = idMap.getOrDefault(id, "");
            if(inMap.isEmpty())
                idMap.put(id, id);
            else
                count++;
        }

        reader = new FileReader("docs/debateorg.json");
        p = new RckToucheParser(reader);
        for(ParsedDocument d: p){
            String id = d.getId();
            String inMap = idMap.getOrDefault(id, "");
            if(inMap.isEmpty())
                idMap.put(id, id);
            else
                count++;
        }

        System.out.println(count);
        /*
        int count = 0;
        for(ParsedDocument d: p){
            count ++;
            System.out.printf("%s%n", d.toString());
        }
        System.out.println(count);

         */
    }

}
