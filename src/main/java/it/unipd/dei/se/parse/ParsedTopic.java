package it.unipd.dei.se.parse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents a parsed topic.
 */
public class ParsedTopic {
    private final String number;
    private final String title;
    private final String description;
    private final String narrative;

    /**
     * Create a new parsed topic.
     * @param n the number of the topic.
     * @param t the title of the topic.
     * @param d the description of the topic.
     * @param nr the narrative of the topic.
     */
    public ParsedTopic(String n, String t, String d, String nr){
        this.number = n;
        this.title = t;
        this.description = d;
        this.narrative = nr;
    }

    /**
     * Gets the topic's number.
     * @return the topic's number.
     */
    public String getNumber(){
        return number;
    }

    /**
     * Gets the topic's title.
     * @return the topic's title.
     */
    public String getTitle(){
        return title;
    }

    /**
     * Gets the topic's description.
     * @return the topic's description.
     */
    public String getDescription(){
        return description;
    }

    /**
     * Gets the topic's narrative.
     * @return the topic's narrative.
     */
    public String getNarrative(){
        return narrative;
    }

    /**
     * Get topic fields in a printable format.
     * @return the fields in a printable and readable format.
     */
    @Override
    public final String toString(){
        ToStringBuilder tsb = new ToStringBuilder(
                this, ToStringStyle.MULTI_LINE_STYLE)
                .append("number", number)
                .append("title", title)
                .append("description", description)
                .append("narrative", narrative);

        return tsb.toString();
    }
}
