/*
 *  Copyright 2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.lucene.document.Field;

/**
 * Represents a parsed document to be indexed.
 */
public class ParsedDocument {

    /**
     * The names of the {@link Field}s within the index.
     *
     */
    public final static class FIELDS {

        /**
         * The document identifier
         */
        public static final String ID = "id";
        /**
         * The document body
         */
        public static final String BODY = "body";

        public static final String PREMISES = "premises";

        public static final String STANCE = "stance";
    }

    private final String id;
    private final String body;
    private final String conclusion;
    private final String stance;
    private final String sourceTitle;
    private final String discussionTitle;
    private final String premisesText;
    private final String authorRole;
    private final String author;
    private final String authorOrg;
    private final String sourceText;
    private final String mode;
    private final String topic;
    private final String sourceDomain;


    /**
     * Creates a new parsed document
     * @param id unique identifier of the document
     * @param body the whole text of the document //TODO: BODY SHOULD BE CLEANED
     * @param conclusion the conclusions for the document
     * @param stance the stance regarding the conclusion
     * @param sourceTitle the title of the document (as displayed in the source website)
     * @param discussionTitle the title of the discussion
     * @param premisesText the text of the premise to the conclusion
     * @param authorRole the role of the author
     * @param authorOrg the organization or political party of the author
     * @param author first and last name of the author
     * @param sourceText the whole text of the document
     * @param mode TODO:NOT SURE
     * @param topic the topic of discussion
     * @param sourceDomain the domain of the source eg: idebate.org/
     */
    public ParsedDocument(final String id,
                          final String body,
                          final String conclusion,
                          final String stance,
                          final String sourceTitle,
                          final String discussionTitle,
                          final String premisesText,
                          final String authorRole,
                          final String authorOrg,
                          final String author,
                          final String sourceText,
                          final String mode,
                          final String topic,
                          final String sourceDomain
                          )
    {
        if(id == null){
            throw new NullPointerException("id cannot be null");
        }
        if(id.trim().isEmpty()){
            throw new IllegalArgumentException("id cannot be empty");
        }
        this.id = id;

        if(body == null){
            throw new NullPointerException("body cannot be null");
        }
        if(body.trim().isEmpty()){
            throw new IllegalArgumentException("body cannot be empty");
        }
        this.body = body;
        this.conclusion = conclusion;
        this.stance = stance;
        this.sourceTitle = sourceTitle;
        this.discussionTitle = discussionTitle;
        this.premisesText = premisesText;
        this.authorRole = authorRole;
        this.authorOrg = authorOrg;
        this.author = author;
        this.sourceText = sourceText;
        this.mode = mode;
        this.topic = topic;
        this.sourceDomain = sourceDomain;
    }

    /**
     * Returns the unique document identifier.
     *
     * @return the unique document identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the body of the document.
     *
     * @return the body of the document.
     */
    public String getBody() {
        return body;
    }

    /**
     * Returns the stance of the document.
     * @return the stance of the document.
     */
    public String getStance(){
        return stance;
    }

    /**
     * Returns the source title of the document.
     * @return the source title of the document.
     */
    public String getSourceTitle(){
        return sourceTitle;
    }

    /**
     * Returns the discussion title of the document.
     * @return the discussion title of the document.
     */
    public String getDiscussionTitle(){
        return discussionTitle;
    }

    /**
     * Returns the content of the premises.
     * @return the content of the premises.
     */
    public String getPremisesText(){
        return premisesText;
    }

    /**
     * Returns the author's role.
     * @return the author's role.
     */
    public String getAuthorRole() {
        return authorRole;
    }

    /**
     * Returns the author's first and last names.
     * @return the author's first and last names.
     */
    public String getAuthor(){
        return author;
    }

    /**
     * Returns the author's organization.
     * @return the author's organization.
     */
    public String getAuthorOrg(){
        return authorOrg;
    }

    /**
     * Returns the uncleaned source text.
     * @return the uncleaned source text.
     */
    public String getSourceText(){
        return sourceText;
    }

    /**
     * Returns the document's mode.
     * @return the document's mode.
     */
    public String getMode(){
        return mode;
    }

    /**
     * Returns the discussion topic.
     * @return the discussion topic.
     */
    public String getTopic(){
        return topic;
    }

    /**
     * Returns the source domain.
     * @return the source domain.
     */
    public String getSourceDomain(){
        return sourceDomain;
    }

    /**
     * Returns the document in a printable format.
     * @return the document's fields in a printable format.
     */
    @Override
    public final String toString() {
        ToStringBuilder tsb = new ToStringBuilder(
                this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("body", body)
                .append("conclusion", conclusion)
                .append("stance", stance)
                .append("source-title", sourceTitle)
                .append("discussion-title", discussionTitle)
                .append("premises-text", premisesText)
                .append("author-role", authorRole)
                .append("author", author)
                .append("author-org", authorOrg)
                .append("source-text", sourceText)
                .append("mode", mode)
                .append("topic", topic)
                .append("source-domain", sourceDomain);

        return tsb.toString();
    }

    @Override
    public final boolean equals(Object o) {
        return (this == o) || ((o instanceof ParsedDocument) && id.equals(((ParsedDocument) o).id));
    }

    @Override
    public final int hashCode() {
        return 37 * id.hashCode();
    }

}
