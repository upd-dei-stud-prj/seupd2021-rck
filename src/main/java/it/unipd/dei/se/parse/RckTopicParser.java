package it.unipd.dei.se.parse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;

/**
 * Represents a parser for topics.
 */
public class RckTopicParser extends TopicParser{
    //the current parsed topic
    private ParsedTopic topic = null;
    //represents the topics in the xml file, each one having root <topic>
    private final NodeList nodes;
    //the total number of topics
    private final int size;
    //the pointer to the current topic
    private int curr;
    /**
     * Creates a new topic parser.
     * @param reader the reader to the document(s) to be parsed.
     * @throws NullPointerException if {@code reader} is {@code null}.
	 * @throws IOException if something went wrong in I/O operation.
	 * @throws SAXException if there's error in parsing.
	 * @throws ParserConfigurationException if an error happens in configuration.
     */
    protected RckTopicParser(Reader reader) throws ParserConfigurationException, IOException, SAXException {

        super(new BufferedReader(reader));

        //Parse topics
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document topics = db.parse(new InputSource(reader));

        //get the set of topics
        topics.getDocumentElement().normalize();
        nodes = topics.getElementsByTagName("topic");
        size = nodes.getLength();

        //points to first topic
        curr = 0;
    }

    @Override
    public boolean hasNext(){
        if(curr >= size)
            return false;

        //Access the next topic
        Element element = (Element) nodes.item(curr);
        curr++;

        //Extract fields
        String number = element.getElementsByTagName("number").item(0).getTextContent();
        String title = element.getElementsByTagName("title").item(0).getTextContent();
        String description = "Nothing";
        String narrative = "Nothing";
        NodeList descNodeList = element.getElementsByTagName("description");
        if(descNodeList != null && descNodeList.getLength() > 0)
            description = element.getElementsByTagName("description").item(0).getTextContent();

        NodeList narrNodeList = element.getElementsByTagName("narrative");
        if(narrNodeList != null && narrNodeList.getLength() > 0)
            narrative = element.getElementsByTagName("narrative").item(0).getTextContent();

        topic = new ParsedTopic(number, title, description, narrative);
        return true;
    }

    @Override
    protected final ParsedTopic parse(){
        return topic;
    }

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        Reader reader = new FileReader("topics/topics-task-1.xml");
        RckTopicParser p = new RckTopicParser(reader);

        int count = 0;
        for(ParsedTopic t: p){
            count ++;
            System.out.printf("%s%n", t.getTitle());
        }
        System.out.println(count);
    }
}
