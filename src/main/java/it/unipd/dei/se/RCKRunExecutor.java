package it.unipd.dei.se;


import it.unipd.dei.se.analysis.*;
import it.unipd.dei.se.index.DirectoryIndexer;
import it.unipd.dei.se.parse.RckToucheParser;
import it.unipd.dei.se.search.Searcher;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;

import java.util.*;


public class RCKRunExecutor {

    /**
     * Main method of the class. Tries different settings of analyzer.
     * 
	 * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing and searching.
     */
    public static void main(String[] args) throws Exception {

        final int ramBuffer = 1024;
        final String docsPath = "docs";
        final String extension = "json";
        final int expectedDocs = 387740;
        final String charsetName = "ISO-8859-1";

        final int maxDocsRetrieved = 1000;
        final int expectedTopics = 50;
        final String runPath = "experiment";

        //final String[] topicsFiles = {"topics/topics-task-1.xml", "topics/topics-expanded-v2.xml"};

        //garbace collection overhead limit exceeded error with this set, so it's commented out forever
        //final String[] topicsFiles = {"topics/topics-task-1-only-titles.xml", "topics/topics-task-1-only-titles-expanded-v2.xml"};

        final String[] topicsFiles = {"topics/topics-task-1.xml"};

        //Similarity[] similarities = {new LMDirichletSimilarity(1800), new LMDirichletSimilarity(2000)};
        //Similarity[] similarities = {new LMDirichletSimilarity(1800), new LMDirichletSimilarity(2000), new LMDirichletSimilarity(1500)};
        Similarity[] similarities = {new LMDirichletSimilarity(1800)};
        final int[] shingleSizes = {0};
        final boolean doIndex = false;

        List<AnalyzerContainer> analyzerList = new ArrayList<>();


        for(Integer size: shingleSizes){
            boolean doShingle = size > 0;
            for(Similarity similarity: similarities){
                float mu = ((LMDirichletSimilarity)similarity).getMu();
                String id = "stop-kstem-doShingle-" + doShingle + "-shingle-size-" + size + "-Dirichlet-mu-" + mu;
                analyzerList.add(new AnalyzerContainer(new RCKAnalyzer(size, doShingle), similarity, id, doIndex));
            }
        }

        for (AnalyzerContainer analyzerContainer : analyzerList) {

            final String indexPath = "experiment/index-" + analyzerContainer.getDescription();

            Analyzer a = analyzerContainer.getAnalyzer();

            if (analyzerContainer.doIndex()) {
                // indexing
                DirectoryIndexer i = new DirectoryIndexer(
                        a, //analyzer
                        analyzerContainer.getSimilarity(), //similarity
                        ramBuffer, //buffersize
                        indexPath, //path to store the index
                        docsPath, //path where documents are stored
                        extension, //documents extension
                        charsetName, //encoding of documents
                        expectedDocs, //number of expected documents
                        RckToucheParser.class //parser class
                );
                i.index();
            }
            // searching
            for(String topics: topicsFiles) {
                String runID = "new-seupd2021-rck-" + analyzerContainer.getDescription();
                if(topics.contains("expanded"))
                    runID += "-expanded";
                if(topics.contains("only-titles"))
                    runID += "-topics-2021";
                Searcher s = new Searcher(
                        //new RCKQueryAnalyzer(0, false),
						a,
                        analyzerContainer.getSimilarity(),
                        indexPath,
                        topics,
                        expectedTopics,
                        runID, runPath,
                        maxDocsRetrieved
                );
                s.search();
            }
        }
    }
}
