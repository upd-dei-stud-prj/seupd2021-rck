package it.unipd.dei.se.analysis;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;


/**
 * Represents an operation to do on  RCKRunExecutor
 *
 */

public class AnalyzerContainer {

    /**
	 * The Analyze to user 
	 */
	private final Analyzer analyzer;
	
	/**
	 * The similarity function to use
	 */
    private final Similarity similarity;

    /**
	 * Name of the run that will be used for naming index and run file
	 */
    private final String description;

    /**
	 * Bool that tell the program if we have to redo index or only searching
	 */
    private final boolean doIndex;

    /**
	 * Creates a new operation
	 *
	 *@param analyzer
	 *       Analyzer to use
     *@param similarity
     *       Similarity to use
     *@param description
	 *       Description of operation
	 *@param doIndex
	 *       Redo index or not
	 */
    public AnalyzerContainer(Analyzer analyzer, Similarity similarity, String description, boolean doIndex){
        this.analyzer = analyzer;
        this.similarity = similarity;
        this.description = description;
        this.doIndex = doIndex;
    }

    /**
	 * Return description of operation
	 *
	 * @return description of the operation
	 */
    public String getDescription() {
        return description;
    }
 
    /**
	 * Return the analyzer of the operation
	 *
	 * @return analyzer
	 */
    public Analyzer getAnalyzer(){
        return analyzer;
    }
 
    /**
	 * Return the similarity function used in the operation
	 *
	 * @return similarity function used
	 */
    public Similarity getSimilarity(){
        return similarity;
    }
    
	/**
	 * Return value of doIndex
	 *
	 * @return doIndex
	 */

    public boolean doIndex(){
        return doIndex;
    }
}
