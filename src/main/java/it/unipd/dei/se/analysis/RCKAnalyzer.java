package it.unipd.dei.se.analysis;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.Reader;

import static it.unipd.dei.se.analysis.AnalyzerUtil.loadStopList;


/**
 * Analyzer built for task 1 in Touchè {@link Analyzer} by using different {@link Tokenizer}s and {@link
 * org.apache.lucene.analysis.TokenFilter}s.
 *
 */

public class RCKAnalyzer extends Analyzer {

    /**
     * Creates a new instance of the analyzer.
     */

    private final int shingleNumber;
    private final boolean doShingle;
    public RCKAnalyzer(int shingleSize) {
        this(shingleSize, true);
    }

    public RCKAnalyzer(int shingleSize, boolean doShingle){
        super();
        this.shingleNumber = shingleSize;
        this.doShingle = doShingle;
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {

        final Tokenizer source = new StandardTokenizer();

        TokenStream tokens = new EnglishPossessiveFilter(source);

        tokens = new LowerCaseFilter(tokens);

        tokens = new StopFilter(tokens, loadStopList("smart.txt"));

        tokens = new KStemFilter(tokens);

        if(doShingle)
            tokens = new ShingleFilter(tokens, this.shingleNumber);

        return new TokenStreamComponents(source, tokens);
    }

    @Override
    protected Reader initReader(String fieldName, Reader reader) {
        // return new HTMLStripCharFilter(reader);

        return super.initReader(fieldName, reader);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }


}