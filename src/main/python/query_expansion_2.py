import string
import nltk
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
import os


def get_pos(tag):
    print(tag)
    if tag[1].startswith('J'):
        return wordnet.ADJ
    if tag[1].startswith('V'):
        return wordnet.VERB
    if tag[1].startswith('N'):
        return wordnet.NOUN
    if tag[1].startswith('R'):
        return wordnet.ADV

    return ''

os.chdir("..")
os.chdir("..")
os.chdir("../topics")
print(os.getcwd())
### LAST BEST: 2 SYNONYMS
# the file containing just the text of every topic title
f = open("new-topic-titles.txt", "r")

# the output file
fout = open("topics-task-1-only-titles-expanded-v2.xml", "w", encoding="utf-8")
stop_words = set(stopwords.words("english"))

stemmer = SnowballStemmer("english")

# write first tag of the document
fout.write("<topics>\n")

# number of the current topic
topic_number = 51
while 1:
    line = f.readline()
    if not line:
        break

    print(f'Original line: {line}')

    # clean the title
    line = line.replace('\n', '')
    line = line.lower()

    # removes all punctuation
    line = line.translate(str.maketrans('', '', string.punctuation))

    # tokenize
    word_tokens = word_tokenize(line)

    # remove stopwords
    filtered = [word for word in word_tokens if not word in stop_words]

    print(f'Tokens: {word_tokens}')

    # pos tagging of query
    pos = nltk.pos_tag(filtered)

    # we expand by adding synonyms
    synonyms = []

    itemIndex = 0
    count = 0
    for item in filtered:

        syns = wordnet.synsets(item)

        # if we didn't find any synonyms, try again after stemming
        if not syns:
            syns = wordnet.synsets(stemmer.stem(item))

        item_POS = get_pos(pos[itemIndex])

        for s in syns:
            if str(s.pos()) == str(item_POS):
                for l in s.lemmas():
                    if(count < 2):
                        if l.name().replace("_", " ") not in synonyms:
                            synonyms.append(l.name().replace("_", " "))
                            count += 1

        count = 0
        itemIndex += 1

    synonyms_string = ' '.join(synonyms)
    new_line = " ".join([str(line), synonyms_string])
    synonyms.clear()
    print(new_line)
    fout.write("<topic>\n")
    fout.write("<number>" + str(topic_number) + "</number>\n")
    topic_number += 1
    # topics miss a topic....
    if topic_number == 25:
        topic_number += 1

    fout.write("<title>")
    fout.write(new_line)
    fout.write("</title\n>")

    fout.write("<description>\nEMPTY\n</description>\n")
    fout.write("<narrative>\nEMPTY\n</narrative>\n")

    fout.write("</topic>\n")
    print()
    # fout.write(new_line)
    # fout.write('\n')

fout.write("</topics>\n")

fout.close()
f.close()

