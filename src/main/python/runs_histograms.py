import matplotlib.pyplot as plt
import numpy as np
import fileinput
import matplotlib.patches as mpatches

runs_ndcg_5 = []
runs_ndcg_10 = []

# read run results and parse it
for line in fileinput.input():
    if line.startswith("ndcg_cut_5"):
        result = line.split("\t")[-1].replace("\n", "")
        runs_ndcg_5.append(float(result))
    if line.startswith("ndcg_cut_10"):
        result = line.split("\t")[-1].replace("\n", "")
        runs_ndcg_10.append(float(result))


run_labels = [i for i in range(1, len(runs_ndcg_5) + 1)]
ax = plt.subplot(111)

LABEL_FONT_SIZE = 16
TICK_FONT_SIZE = 12
LEGEND_FONT_SIZE = 12
COLOR_1 = "lightblue"
COLOR_2 = "salmon"

for label in run_labels:
    ax.bar(label - 0.2, runs_ndcg_5[label - 1], width=0.4, color=COLOR_1, align='center', label="cut_5")
    ax.bar(label + 0.2, runs_ndcg_10[label - 1], width=0.4, color=COLOR_2, align='center', label="cut_10")


ax.set_yticks(np.arange(0, 0.7, 0.05))
plt.title("Runs", fontsize=LABEL_FONT_SIZE)
plt.xlabel("Run number", fontsize=LABEL_FONT_SIZE)
plt.xticks(fontsize=TICK_FONT_SIZE)
plt.yticks(fontsize=TICK_FONT_SIZE)
patch_1 = mpatches.Patch(color=COLOR_1, label="ndcg_cut@5")
patch_2 = mpatches.Patch(color=COLOR_2, label="ndcg_cut@10")
plt.legend(handles=[patch_1, patch_2], fontsize=LEGEND_FONT_SIZE)
plt.show()