import matplotlib.pyplot as plt
import numpy as np
import fileinput
import matplotlib.patches as mpatches

indexing_times = []

for line in fileinput.input():
    t = line.split(" ")[-2]
    indexing_times.append(int(t))

labels = [i for i in range(1, len(indexing_times) + 1)]

ax = plt.subplot(111)

LABEL_FONT_SIZE = 16
TICK_FONT_SIZE = 12
LEGEND_FONT_SIZE = 12

COLOR_1 = "#B0DFE5"
COLOR_2 = "#95C8D8"
COLOR_3 = "#89CFF0"
COLOR_4 = "#4682B4"
COLOR_5 = "#7285A5"

colors = [COLOR_1, COLOR_2, COLOR_3, COLOR_4, COLOR_5]

for label in labels:
    ax.bar(label, indexing_times[label - 1], width=0.4, color=colors[label - 1], align="center")

ax.set_yticks(np.arange(0, max(indexing_times), 100))
plt.title("Indexing times", fontsize=LABEL_FONT_SIZE)
plt.xlabel("Index number", fontsize=LABEL_FONT_SIZE)
plt.xticks(fontsize=TICK_FONT_SIZE)
plt.yticks(fontsize=TICK_FONT_SIZE)
plt.show()