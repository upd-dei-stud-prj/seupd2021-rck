import string
from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import os

os.chdir("..")
os.chdir("..")
os.chdir("../topics")
print(os.getcwd())
# the file containing just the text of every topic title
f = open("old-topic-titles.txt", "r")

# the output file
fout = open("topics-expanded-v1.xml", "w", encoding="utf-8")
stop_words = set(stopwords.words("english"))

# write first tag of the document
fout.write("<topics>\n")

# number of the current topic
topic_number = 1
while 1:
    line = f.readline()
    if not line:
        break

    print(f'Original line: {line}')

    # clean the title
    line = line.replace('\n', '')
    line = line.lower()

    # removes all punctuation
    line = line.translate(str.maketrans('', '', string.punctuation))
    word_tokens = word_tokenize(line)

    # remove stopwords
    filtered = [word for word in word_tokens if not word in stop_words]

    print(f'Tokens: {word_tokens}')

    # we expand by adding synonyms and antonyms
    synonyms_antonyms = []

    count = 0
    # for each word
    for x in filtered:
        # get list of synonyms
        for syn in wordnet.synsets(x):
            for lemma in syn.lemmas():
                # add at most 2 synonyms
                if count < 2:
                    if lemma.name() not in synonyms_antonyms:
                        synonyms_antonyms.append(lemma.name().replace("_", " "))
                        print(f'Lemma: {lemma.name()}')
                        # synonyms_antonyms.append(lemma.name())
                        count += 1


                        # add only 1 antonym, if present
                        if lemma.antonyms():
                            print(f'Lemma: {lemma.name()}, Antonym: {lemma.antonyms()[0].name()}')
                            # synonyms_antonyms.append(lemma.antonyms()[0].name())
                            synonyms_antonyms.append(lemma.antonyms()[0].name().replace("_", " "))

                '''
                if lemma.antonyms():
                    print(f'Lemma: {lemma.name()}, Antonym: {lemma.antonyms()[0].name()}')
                    # synonyms_antonyms.append(lemma.antonyms()[0].name())
                    synonyms_antonyms.append(lemma.antonyms()[0].name().replace("_", ""))
                '''

        count = 0

    synonyms_string = ' '.join(synonyms_antonyms)
    new_line = " ".join([str(line), synonyms_string])
    synonyms_antonyms.clear()
    print(new_line)
    fout.write("<topic>\n")
    fout.write("<number>" + str(topic_number) + "</number>\n")
    topic_number += 1
    # topics miss a topic....
    if topic_number == 25:
        topic_number += 1

    fout.write("<title>")
    fout.write(new_line)
    fout.write("</title\n>")

    fout.write("<description>\nEMPTY\n</description>\n")
    fout.write("<narrative>\nEMPTY\n</narrative>\n")

    fout.write("</topic>\n")
    print()
    # fout.write(new_line)
    # fout.write('\n')

fout.write("</topics>\n")

fout.close()
f.close()
