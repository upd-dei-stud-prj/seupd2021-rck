'''

import glob

#NDCG_10
#get run files
runFiles = glob.glob('ndcg-10*.txt')

#list will contain the map topicN --> score for each of the runfiles
#topics are not sorted so we use a map to sort them ourselves
runs = []

for runFile in runFiles:
	print(runFile)
	with open(runFile, 'r') as f:
		map = {}
		for line in f.readlines():
			#clean line, split in topicN, score
			line = line.replace('\n', '')
			if line == '':
				continue
			els = line.split('\t')
			topicN = int(els[0])
			score = float(els[1])

			#insert into map
			map[topicN] = score

		#append map to runs list
		runs.append(map)

with open('ndcg-10.csv', 'w') as f:
	#write in each line the topic score, each column is a run
	for i in range(1, 51):
		row = ""
		for run in runs:
			score = 0
			#very bad runs can't even retrieve 1 singular relevant document
			#hence trec_eval does not report the score for that topic

			#also, 2020 topics for some reason miss topic number 25
			try:
				score = run[i]
			except:
				pass
			row += str(score) + ','

		#we remove tailing comma, we add newline
		row = row[0: -1]
		row += '\n'
		print(f'{i} --> {row}')
		f.write(row)

'''

def make_csv(measure):
	import glob

	#NDCG_10
	#get run files
	runFiles = glob.glob(measure + '*.txt')

	#sort run files to have the same order of runs for every measure
	#provided run file names do not change
	runFiles.sort()

	#list will contain the map topicN --> score for each of the runfiles
	#topics are not sorted so we use a map to sort them ourselves
	runs = []

	for runFile in runFiles:
		print(runFile)
		with open(runFile, 'r') as f:
			map = {}
			for line in f.readlines():
				#clean line, split in topicN, score
				line = line.replace('\n', '')
				if line == '':
					continue
				els = line.split('\t')
				topicN = int(els[0])
				score = float(els[1])

				#insert into map
				map[topicN] = score

			#append map to runs list
			runs.append(map)

	with open(measure + '.csv', 'w') as f:
		#write in each line the topic score, each column is a run
		for i in range(1, 51):
			row = ""
			for run in runs:
				score = 0
				#very bad runs can't even retrieve 1 singular relevant document
				#hence trec_eval does not report the score for that topic

				#also, 2020 topics for some reason miss topic number 25
				try:
					score = run[i]
				except:
					pass
				row += str(score) + ','

			#we remove tailing comma, we add newline
			row = row[0: -1]
			row += '\n'
			f.write(row)



make_csv('ndcg-10')
make_csv('map')
