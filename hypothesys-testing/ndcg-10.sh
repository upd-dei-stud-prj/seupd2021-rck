for runfile in seupd*.txt;
do
rm "ndcg-10 - ${runfile}";
touch "ndcg-10 - ${runfile}";
./trec_eval-9.0.7/trec_eval -q -m ndcg_cut touche2020-task1-relevance-args-me-corpus-version-2020-04-01.qrels "${runfile}" | egrep '(ndcg_cut_10)\s*[0-9]+\s*[0-9]{1}\.[0-9]+' | egrep -o '[0-9]+\s*[0-9]{1}\.[0-9]+' >> "ndcg-10 - ${runfile}"; 
done